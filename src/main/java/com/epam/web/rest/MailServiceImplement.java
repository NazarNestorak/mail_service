package com.epam.web.rest;

import com.epam.bo.LetterBo;
import com.epam.model.Letter;
import com.epam.web.fault.FaultMessage;
import com.epam.web.fault.ServiceFaultInfo;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

public class MailServiceImplement implements MailService {
    private Logger logger = LogManager.getLogger(MailServiceImplement.class);

    @Override
    public Response getAllLetters() {
        logger.info("inside 'getAllLetters' method");
        LetterBo letterBo = new LetterBo();
        return Response.ok().entity(letterBo.getAllLetter()).build();
    }

    @Override
    public Response getLetter(String name) {
        logger.info("inside 'getLetter' method");
        Response response;
        LetterBo letterBo = new LetterBo();
        Letter letter = letterBo.getLetter(name);
        if (Objects.isNull(letter)) {
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.NO_LETTER_WITH_NAME, name);
            logger.error(faultInfo.getMessage());
            response = Response.status(Response.Status.NOT_FOUND).entity(faultInfo).build();
        } else {
            logger.info("method 'getLetter' get: " + letter);
            response = Response.ok().entity(letter).build();
        }
        return response;
    }

    @Override
    public Response addLetter(Letter letter) {
        logger.info("inside 'addLetter' method");
        Response response;
        LetterBo letterBo = new LetterBo();
        if (letterBo.addLetter(letter)) {
            response = Response.ok().build();
            logger.info("method 'addLetter' get: " + letter);
        } else {
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.LETTER_ALREADY_EXIST, letter);
            logger.error(faultInfo.getMessage());
            response = Response.status(Response.Status.NOT_ACCEPTABLE).entity(faultInfo).build();
        }
        return response;
    }

    @Override
    public Response removeLetters(String name) {
        logger.info("inside 'removeLetters' method");
        Response response;
        LetterBo letterBo = new LetterBo();
        if (!letterBo.removeLetter(name)) {
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.NO_LETTER_WITH_NAME, name);
            logger.error(faultInfo.getMessage());
            response = Response.status(Response.Status.NOT_FOUND).entity(faultInfo).build();
        } else {
            response = Response.ok().build();
        }
        return response;
    }

    @Override
    public Response getUserLetters(String email, int number) {
        logger.info("inside 'getUserLetters' method");
        Response response;
        LetterBo letterBo = new LetterBo();
        List<Letter> letterList = letterBo.getLetterByEmail(email);
        if (letterList.size() < number) {
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.NOT_ENOUGH_LETTERS_OF_AUTHOR, letterList.size(), email, number);
            logger.warn(faultInfo.getMessage());
            response = Response.status(Response.Status.NOT_ACCEPTABLE).entity(faultInfo).build();
        } else {
            response = Response.ok().entity(letterList.subList(0, number)).build();
        }
        return response;
    }
}
