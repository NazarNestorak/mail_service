package com.epam.web.rest;

import com.epam.model.Letter;

import javax.jws.WebService;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@WebService
public interface MailService {

    @GET
    @Path("/letters")
    @Produces("application/json; charset=UTF-8")
    Response getAllLetters();

    @GET
    @Path("/letters/{header}")
    @Consumes("text/plain; charset=UTF-8")
    @Produces("application/json; charset=UTF-8")
    Response getLetter(@PathParam("header") String name);

    @POST
    @Path("/letters")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json; charset=UTF-8")
    Response addLetter(Letter letter);

    @DELETE
    @Path("/letters")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json; charset=UTF-8")
    Response removeLetters(String name);

    @GET
    @Path("/letters/users/{email}/{number}")
    @Produces("application/json; charset=UTF-8")
    Response getUserLetters(String email, int number);

}
