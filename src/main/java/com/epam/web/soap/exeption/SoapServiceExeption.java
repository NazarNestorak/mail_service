package com.epam.web.soap.exeption;

import com.epam.web.fault.ServiceFaultInfo;

public class SoapServiceExeption extends Exception {
    private ServiceFaultInfo serviceFaultInfo;

    public SoapServiceExeption(ServiceFaultInfo serviceFaultInfo){
        this.serviceFaultInfo = serviceFaultInfo;
    }

    public ServiceFaultInfo getServiceFaultInfo() {
        return serviceFaultInfo;
    }

    public void setServiceFaultInfo(ServiceFaultInfo serviceFaultInfo) {
        this.serviceFaultInfo = serviceFaultInfo;
    }
}
