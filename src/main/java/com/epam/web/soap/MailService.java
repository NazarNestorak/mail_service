package com.epam.web.soap;

import com.epam.model.Letter;
import com.epam.web.soap.exeption.SoapServiceExeption;

import javax.jws.WebService;
import java.util.List;
@WebService
interface MailService {
    List<Letter> getAllLetters();

    Letter getLetter(String header) throws SoapServiceExeption;

    boolean addLetter(Letter letter) throws SoapServiceExeption;

    boolean removeLetter(String title) throws SoapServiceExeption;

    List<Letter> getUserLetters(String email, int number)throws SoapServiceExeption;

}
