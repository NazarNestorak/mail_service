package com.epam.web.soap;

import com.epam.bo.LetterBo;
import com.epam.model.Letter;
import com.epam.web.fault.FaultMessage;
import com.epam.web.fault.ServiceFaultInfo;
import com.epam.web.soap.exeption.SoapServiceExeption;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService(endpointInterface = "com.epam.web.soap.MailService")
public class MailServiceImplement implements MailService {
    private static Logger logger = LogManager.getLogger(MailServiceImplement.class);

    @Override
    public List<Letter> getAllLetters() {
        logger.info("inside 'getAllLetters' method");
        LetterBo letterBo = new LetterBo();
        return letterBo.getAllLetter();
    }

    @Override
    public Letter getLetter(String header) throws SoapServiceExeption {
        logger.info("inside 'getLetter' method");
        LetterBo letterBo = new LetterBo();
        Letter letter = letterBo.getLetter(header);
        if (Objects.isNull(letter)) {
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.NO_LETTER_WITH_NAME);
            logger.error(faultInfo);
            throw new SoapServiceExeption(faultInfo);
        }
        logger.info("method 'getLetter' get: " + letter);
        return letter;
    }

    @Override
    public boolean addLetter(Letter letter) {
        logger.info("inside 'addLetter' method");
        LetterBo letterBo = new LetterBo();
        boolean isAddLetter;
        if (!letterBo.addLetter(letter)) {
            isAddLetter = false;
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.LETTER_ALREADY_EXIST, letter);
            logger.error(faultInfo.getMessage());
            try {
                throw new SoapServiceExeption(faultInfo);
            } catch (SoapServiceExeption e) {
                logger.warn(faultInfo);
            }
        } else {
            isAddLetter = true;
            logger.info("method 'addLetter' get: " + isAddLetter);
        }
        return isAddLetter;
    }

    @Override
    public boolean removeLetter(String title) throws SoapServiceExeption {
        logger.info("inside 'removeLetter' method");
        LetterBo letterBo = new LetterBo();
        boolean isRemoveLetter;
        if (!letterBo.removeLetter(title)) {
            isRemoveLetter = false;
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.NO_LETTER_WITH_NAME, title);
            logger.error(faultInfo.getMessage());
            throw new SoapServiceExeption(faultInfo);
        } else {
            isRemoveLetter = true;
            logger.info("method 'removeLetter' get: " + isRemoveLetter);
        }
        return isRemoveLetter;
    }

    @Override
    public List<Letter> getUserLetters(String email, int amountBooks) throws SoapServiceExeption {
        logger.info("inside 'getUserLetters' method");
        LetterBo letterBo = new LetterBo();
        List<Letter> letterList = letterBo.getLetterByEmail(email);
        if (letterList.size() < amountBooks) {
            ServiceFaultInfo faultInfo = new ServiceFaultInfo(FaultMessage.NOT_ENOUGH_LETTERS_OF_AUTHOR, letterList.size(), email, amountBooks);
            logger.error(faultInfo.getMessage());
            throw new SoapServiceExeption(faultInfo);
        }
        return letterList.subList(0,amountBooks);
    }
}
