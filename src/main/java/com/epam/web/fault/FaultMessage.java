package com.epam.web.fault;

public enum  FaultMessage {
    NO_LETTER_WITH_NAME("There is no letter with  name [%s]"),
    NOT_ENOUGH_LETTERS_OF_AUTHOR("There are only [%d] letters of [%s], [%d] have bean required"),
    LETTER_ALREADY_EXIST("[%s] letter already exist");

    private String messageExpression;

    private FaultMessage(String message) {
        this.messageExpression = message;
    }

    public String getMessageExpression() {
        return this.messageExpression;
    }
}
