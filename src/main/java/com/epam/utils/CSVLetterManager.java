package com.epam.utils;

import com.epam.model.Letter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CSVLetterManager {
    public static String[] HEADERS = {"Email", "Header", "Text"};
    public static Logger LOG = Logger.getLogger(CSVLetterManager.class);

    /**
     * Method is handel csv file encoding issue
     */
    private static InputStreamReader newReader(final InputStream inputStream) {
        return new InputStreamReader(new BOMInputStream(inputStream), StandardCharsets.UTF_8);
    }

    public static List<Letter> readLetters(File file) {
        List<Letter> list = new ArrayList<>();
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            Iterable<CSVRecord> parser = CSVFormat.DEFAULT.withHeader(HEADERS).withSkipHeaderRecord(true).parse(newReader(in));
            for (CSVRecord record : parser) {
                list.add(new Letter(record.get("Email"), record.get("Header"), record.get("Text")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static void writeLetters(List<Letter> letterList, File file) {
        Writer out = null;
        CSVPrinter csvFilePrinter = null;
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n").withNullString("");
        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            csvFilePrinter = new CSVPrinter(out, csvFileFormat);
            csvFilePrinter.printRecord(HEADERS);
            for (Letter letter : letterList) {
                csvFilePrinter.printRecord(letter.getEmail(),letter.getHeader(),letter.getText());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                out.flush();
                out.close();
                csvFilePrinter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
