package com.epam.dao;

import com.epam.model.Letter;
import com.epam.utils.CSVLetterManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import static com.epam.helper.Constanta.LETTER_URl;

public class LetterDao {
    public static Logger logger = LogManager.getLogger(LetterDao.class);

    public static List<Letter> findAll() {
        return CSVLetterManager.readLetters(new File(LETTER_URl));
    }

    public static Letter findByEmail(String name) {
        logger.info("method: findByEmail");
        List<Letter> letterList = findAll();
        Letter myLetter = null;
        for (Letter letter : letterList) {
            if (letter.getEmail().equals(name)) {
                myLetter = letter;
            }
        }
        return myLetter;
    }

    public static Letter findByHeader(String name) {
        logger.info("method: findByHeader");
        List<Letter> letterList = findAll();
        Letter myLetter = null;
        for (Letter letter : letterList) {
            if (letter.getHeader().equalsIgnoreCase(name)) {
                myLetter = letter;
            }
        }
        return myLetter;
    }

    public static boolean insertLetter(Letter letter) {
        logger.info("method: insertLetter");
        List<Letter> letterList = findAll();
        boolean isAdded;
        if (!letterList.contains(letter)) {
            letterList.add(letter);
            CSVLetterManager.writeLetters(letterList, new File(LETTER_URl));
            isAdded = true;
        } else {
            isAdded = false;
        }
        return isAdded;
    }

    public static boolean deleteLetter(String header) {
        logger.info("method: deleteLetter");
        List<Letter> letterList = findAll();
        ListIterator<Letter> letterIterator = letterList.listIterator();
        boolean isDelete = false;
        if (!Objects.isNull(findByHeader(header))) {
            while (letterIterator.hasNext()) {
                Letter letter = letterIterator.next();
                if (letter.getHeader().equals(header)) {
                    letterIterator.remove();
                    CSVLetterManager.writeLetters(letterList, new File(LETTER_URl));
                    isDelete = true;
                    break;
                }
            }
        }
        return isDelete;
    }
}









