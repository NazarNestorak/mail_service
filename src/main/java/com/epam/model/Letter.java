package com.epam.model;

import java.util.Objects;

public class Letter {
    private String email;
    private String header;
    private String text;

    public Letter() {
    }

    public Letter(String email, String header, String text) {
        this.email = email;
        this.header = header;
        this.text = text;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Letter)) return false;
        Letter letter = (Letter) o;
        return Objects.equals(email, letter.email) &&
                Objects.equals(header, letter.header) &&
                Objects.equals(text, letter.text);
    }

    @Override
    public int hashCode() {

        return Objects.hash(email, header, text);
    }

    @Override
    public String toString() {
        return "Letter{" +
                "email='" + email + '\'' +
                ", header='" + header + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
