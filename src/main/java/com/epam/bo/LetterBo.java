package com.epam.bo;

import com.epam.dao.LetterDao;
import com.epam.model.Letter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class LetterBo {
    public static Logger logger = LogManager.getLogger(LetterBo.class);

    public List<Letter> getAllLetter() {
        return LetterDao.findAll();
    }

    public Letter getLetter(String email) {
        return LetterDao.findByEmail(email);
    }

    public List<Letter> getLetterByEmail(String email) {
        List<Letter> letterList = getAllLetter();
        List<Letter> emailLetterList = new ArrayList<>();
        for (int i = 0; i < letterList.size(); i++) {
            if (letterList.get(i).getEmail().equals(email)) {
                emailLetterList.add(letterList.get(i));
            }
        }
        return emailLetterList;
    }

    public boolean addLetter(Letter letter) {
        return LetterDao.insertLetter(letter);
    }

    public boolean removeLetter(String header) {
        return LetterDao.deleteLetter(header);
    }

}
